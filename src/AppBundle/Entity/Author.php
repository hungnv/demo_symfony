<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/15/2019
 * Time: 10:54 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Author
{
    /**
     * @Assert\NotBlank
     */
    public $name;

}