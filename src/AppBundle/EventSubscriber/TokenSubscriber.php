<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/26/2019
 * Time: 1:52 PM
 */

namespace AppBundle\EventSubscriber;


use AppBundle\Controller\ITokenBaseController;
use AppBundle\Controller\ITokenController;
use AppBundle\Exceptions\MyException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TokenSubscriber implements EventSubscriberInterface
{

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof ITokenBaseController) {
//            $token = $event->getRequest()->query->get('token');
//            if (!in_array($token, $this->tokens)) {
                throw new MyException('This action needs a valid token!');
//            }
        }
    }

    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}