<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 4:50 PM
 */

namespace AppBundle\Repository;


use AppBundle\Repository\Interfaces\IBaseRepository;
use Doctrine\ORM\EntityManagerInterface;

abstract class BaseRepository implements IBaseRepository
{

    protected $repository;

    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {

        $this->repository = $entityManager->getRepository($this->model());

        $this->entityManager=$entityManager;
    }

    /** find all by Id
     * @param $id
     * @return null|object
     */
    public function findById($id)
    {
        return $this->repository->find($id);
    }

    /** find all
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function insert($data)
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public abstract function model();

    public function update($data)
    {
        $this->entityManager->flush();
    }

    /** find by criteria
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /** find one by criteria
     * @param array $criteria
     * @return null|object
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    public function delete($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}