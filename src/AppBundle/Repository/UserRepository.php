<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 4:55 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Repository\Interfaces\IProductRepository;
use AppBundle\Repository\Interfaces\IUserRepository;

class UserRepository extends BaseRepository implements IUserRepository
{

    public function model()
    {
        // TODO: Implement model() method.
        return User::class;
    }

    public function findByEmail($email)
    {
        // TODO: Implement findByEmail() method.
        return $this->repository->findOneBy(['email'=>$email]);

    }

}