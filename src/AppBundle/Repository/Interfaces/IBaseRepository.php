<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 4:50 PM
 */

namespace AppBundle\Repository\Interfaces;


interface IBaseRepository
{
    public function findById($id);

    public function findAll();

    public function insert($data);

    public function update($data);

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    public function findOneBy(array $criteria);

    public function delete($entity);
}