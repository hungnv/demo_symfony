<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 4:55 PM
 */

namespace AppBundle\Repository\Interfaces;


interface IUserRepository extends IBaseRepository
{
    public function findByEmail($email);
}