<?php
/**
 * Created by PhpStorm.
 * User: HUNG
 * Date: 7/23/2019
 * Time: 9:27 PM
 */

namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/test")
 * Class TestController
 * @package AppBundle\Controller
 */
class TestController extends Controller
{

    /**
     * @Route("",name="product-create",methods={"POST"})
     */
    public function createAction(Request $request)
    {
        return $this->json($request->request->all());
    }

    /**
     * @Route("",name="product.index",methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        $name=$request->get("name","");
        return $this->json(["data"=>"done",'name'=>$name]);
    }
}