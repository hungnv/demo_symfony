<?php

namespace AppBundle\Controller;

use AppBundle\Demo;
use AppBundle\Entity\Author;
use AppBundle\ITest;
use AppBundle\Repositories\Eloquent\UserRepository;
use AppBundle\Test;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * set prefix for all router in Default controller
 * @Route("/default")
 */
class DefaultController extends Controller
{
    private $test;
    private $userRepository;
    public function __construct(ITest $test)
    {
//        $this->userRepository=$userRepository;
        $this->test=$test;
    }

    /**
     * @Route("/abc", name="homepage")
     */
    public function indexAction(Request $request)
    {
        dump($this->get('twig'));
        die();
        //get request param in controller
        throw new BadRequestHttpException("daa");
        echo 1223;
        echo $this->getParameter("mail_name");
        echo $this->getParameter("mailer_transport");
        echo $this->getParameter("locale");
        echo $this->getParameter("mailer_password");
        echo $this->test->getAll();
        die();
    }

    /**
     * @Route("/test", name="abc")
     */
    public function test(Request $request)
    {
//        echo 8;die;
        // replace this example code with whatever you need
        dump($this->container->get('twig'));
        exit();
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * Router with default value
     * @Route("/test/param/{name}",name="router-param",defaults={"name"=1})
     * @param string $name
     */

    public function routerParam($name='')
    {

        echo $name;
        die();

    }

    /**
     * @Route("/post",methods={"post"},name="post-request")
     * get param from post,GET request using $request->get('name')
     */
    public function postAction(Request $request)
    {
        //get value form any bag
        echo $request->get("name","");
        echo $request->get("query","");

        //get file upload
        dump($request->files("file"));
        die();

    }

    /**
     * @Route("/store",name="store-demo",methods={"Get"})
     */
    public function storeAction(SessionInterface $session)
    {
        $session->set("key","value");
        echo $session->get("key");

        //flash message in symfony
        $this->addFlash("message","here is the flash message 1");
        $this->addFlash("message","here is the flash message 2");
        return $this->redirectToRoute("flash-message");
    }

    /**
     * @Route("/flash-message",name="flash-message",methods={"GET"})
     * @param SessionInterface $session
     */
    public function flashMessageAction(SessionInterface $session)
    {
        return $this->render("default/session-flash.html.twig");

    }

    /**
     * @Route("/download",name="download-file",methods={"GET"})
     */
    public function downloadAction()
    {
        return $this->file("assets/test.pdf");
    }

    /**
     * @Route("/error",name="error",methods={"GET"})
     * @throws \Exception
     */
    public function errorAction()
    {
        throw new \Exception("Error");
    }

    /**
     * @Route("/validation",name="validation",methods={"GET"})
     * @throws \Exception
     */
    public function validation()
    {
        dump($this->container->get('session'));
        dump($this->container->get('form.factory'));
        echo __();
        die();
        $author=new Author();
        $author->name="";
        $validator=$this->get("validator");
        dump($validator);
//        die();
        $errors=$validator->validate($author);

        foreach ($errors as $error){
            $name=$error->getPropertyPath();
        }
        dump($errors);
        dump($name);
        die();
        return $this->render("default/validator.html.twig",['errors'=>$errors]);

    }




}
