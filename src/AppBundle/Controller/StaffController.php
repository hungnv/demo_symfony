<?php
/**
 * Created by PhpStorm.
 * User: HUNG
 * Date: 7/28/2019
 * Time: 10:25 PM
 */

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/staff")
 * @package AppBundle\Controller
 */
class StaffController implements ITokenBaseController
{
    /**
     * @Route("",name="staff.index",methods={"GET"})
     */
    public function indexAction()
    {
        die("private action");
    }
}