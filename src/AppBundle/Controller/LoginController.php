<?php
/**
 * Created by PhpStorm.
 * User: HUNG
 * Date: 7/23/2019
 * Time: 9:27 PM
 */

namespace AppBundle\Controller;
use AppBundle\Repository\Interfaces\IUserRepository;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/login")
 * Class TestController
 * @package AppBundle\Controller
 */
class LoginController extends Controller
{

    private $userRepository;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository=$userRepository;
    }

    /**
     * @Route("",name="login-form",methods={"GET"})
     */
    public function loginAction(Request $request)
    {
        return $this->render("login/login.html.twig");
    }

    /**
     * @Route("",name="login-action",methods={"POST"})
     */
    public function doLoginAction(Request $request)
    {

        $email=$request->request->get("email",'');
        $user=$this->userRepository->findByEmail($email);

        if($user){
            $key = "example_key";
            $token = array(
                "iss" => "http://example.org",
                "aud" => "http://example.com",
                "exp" => 1564330035,
//                "nbf" => 1357000000,
                "id"=>$user->id

            );

            $jwt = JWT::encode($token, $key);
            var_dump($jwt);


            die();
        }
        dump($user);
        die();
    }

    /**
     * @Route("/verify",name="login-verify",methods={"GET"})
     */
    public function verifyAction(Request $request)
    {
//        die("abc");
        $key="example_key";
        $token=$request->headers->get("Authorization");

        $decode=JWT::decode($token,$key,array('HS256'));
        dump($decode->id);
        die($token);
        die("ddd");

    }


}