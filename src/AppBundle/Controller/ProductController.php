<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 10:47 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Services\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * set prefix for all router in Product controller
 * @Route("/product")
 */
class ProductController extends Controller
{
    protected $productService;
    public function __construct(ProductService $productService)
    {
        $this->productService=$productService;
    }

    /**
     * @Route("",name="product-list",methods={"GET"})
     */
    public function index()
    {
        $productList=$this->productService->getAllProduct();
        dump($productList);
        exit();
        return $this->render("product/index.html.twig");
    }

    /**
     * @Route("",name="product-create",methods={"POST"})
     */
    public function createAction(Request $request)
    {

        return $this->json($request->request->all());

//        $entityManager = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->name = $request->get("name", "");
        $product->price = $request->get("price", "");
        $product->description = $request->get("description", "");

//        $entityManager->persist($product);
//        $entityManager->flush();

        $this->productService->insert($product);
        return new Response("Product save successfull");
    }

    /**
     * @Route("",name="product-update",methods={"PUT"})
     * @param Request $request
     */
    public function updateAction(Request $request)
    {
        $productId=$request->get("id");
        $product=$this->productService->getProductById($productId);

        print_r($product);
        print_r($productId);
        die();

        $product->name=$request->get("name","");
        $product->price=$request->get("price","");
        $product->description=$request->get("description","");


        $this->productService->update($product);
        return new Response("Product update successfull");
//        $product_name->name = $request->get("name", "");
//        $product->price = $request->get("price", "");
//        $product->description = $request->get("description", "");
    }

    /**
     * @Route("/{id}",name="product-delete",methods={"DELETE"})
     * @param Request $request
     */
    public function deleteAction($id,Request $request)
    {
        $product=$this->productService->getProductById($id);
        $this->productService->deleteProduct($product);
        return new Response("Product delete successfull");
    }
}