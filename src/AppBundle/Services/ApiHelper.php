<?php
/**
 * Created by PhpStorm.
 * User: HUNG
 * Date: 7/22/2019
 * Time: 9:43 PM
 */

namespace AppBundle\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Stream\Stream;

class ApiHelper
{
    const API_ENDPOINT = 'http://webservice.local/app_dev.php';

    // These users are defined in API application
    private $authorisation = [
        'student' => 'Basic c3R1ZGVudDpzdHVkZW50',
        'lecturer' => 'Basic bGVjdHVyZXI6bGVjdHVyZXI=',
        'admin' => 'Basic YWRtaW46YWRtaW4=',
    ];

    /**
     * @param string $user
     * @param string $method
     * @param string $uri
     * @param array $getParams
     * @param string|null $postData
     *
     * @return Response
     */
    public function call($user, $method, $uri, array $getParams = [], $postData = null)
    {
        return $this->consume(
            $method,
            $this->createUri($uri, $getParams),
            $this->authorisation[$user],
            $postData
        );
    }

    /**
     * @param string $uri
     * @param array $getParams
     *
     * @return string
     */
    private function createUri($uri, array $getParams = [])
    {
        return sprintf(
            '%s%s?%s',
            self::API_ENDPOINT,
            $uri,
            http_build_query($getParams)
        );
    }

    /**
     * @param string $method
     * @param string $uri
     * @param string $auth
     * @param string|null $postData
     *
     * @return Response
     */
    private function consume($method, $uri, $auth, $postData = null)
    {
        $response = new Response();

        try {
            $client = new Client();
            $request = $client->createRequest($method, $uri);
            $request->addHeader('Authorization', $auth);
            $request->addHeader('Content-Type', 'application/json');
            $request->setBody(Stream::factory($postData));
            $result = $client->send($request);

            $response->code = $result->getStatusCode();
            $response->message = json_decode($result->getBody(), true);
        } catch (BadResponseException $e) {
            $response->code = $e->getCode();
            $response->message = $e->getMessage();
        }

        return $response;
    }
}