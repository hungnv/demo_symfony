FROM hello12/apache_php_7:7.2
WORKDIR /tmp
ADD ./job.sh ./
ADD ./jobs ./jobs
RUN ls
RUN chmod +x /tmp/job.sh
RUN touch log.txt
RUN ls -l
RUN chmod -R 777 /tmp
RUN ls -l
ENTRYPOINT ["/bin/bash","/tmp/job.sh"]